package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

var errUnexpectedRes = errors.New("unexpected response")

func getBody(url string) (body []byte, err error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Get: %w", err)
	}
	defer res.Body.Close()

	body, err = io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("ReadAll: %w", err)
	}

	return
}

func checkStatus(body []byte) error {
	s := struct{ Status string }{}
	if err := json.Unmarshal(body, &s); err != nil {
		return fmt.Errorf("Unmarshal status: %w", err)
	}

	if s.Status != "ok" {
		return fmt.Errorf("unexpected status %q: %w", s.Status, errUnexpectedRes)
	}

	return nil
}

func fetchGuest() (token string, err error) {
	body, err := getBody("https://api.gofile.io/createAccount")
	if err != nil {
		return "", err
	}

	if err := checkStatus(body); err != nil {
		return "", err
	}

	d := struct{ Data struct{ Token string } }{}
	if err := json.Unmarshal(body, &d); err != nil {
		return "", fmt.Errorf("Unmarshal data: %w", err)
	}

	return d.Data.Token, nil
}

func fetchURLs(id, token, websiteToken string) ([]string, error) {
	u := &url.URL{
		Scheme: "https",
		Host:   "api.gofile.io",
		Path:   "/getContent",
	}
	q := u.Query()
	q.Set("contentId", id)
	q.Set("token", token)
	q.Set("websiteToken", websiteToken)
	u.RawQuery = q.Encode()

	body, err := getBody(u.String())
	if err != nil {
		return nil, err
	}

	return parseURLs(body)
}

func parseURLs(body []byte) (out []string, err error) {
	if err := checkStatus(body); err != nil {
		return nil, err
	}

	d := struct {
		Data struct {
			Contents map[string]struct{ Link string }
		}
	}{}
	if err := json.Unmarshal(body, &d); err != nil {
		return nil, fmt.Errorf("Unmarshal data: %w", err)
	}

	for _, v := range d.Data.Contents {
		out = append(out, v.Link)
	}

	return
}

func fetchFile(dst io.Writer, url, token string) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	req.AddCookie(&http.Cookie{Name: "accountToken", Value: token})

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("Get: %w", err)
	}
	defer res.Body.Close()

	_, err = io.Copy(dst, res.Body)
	if err != nil {
		return fmt.Errorf("Copy: %w", err)
	}

	return nil
}
