package main

import (
	"errors"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestParseURLs(t *testing.T) {
	tests := []struct {
		in      string
		want    []string
		wantErr error
	}{
		{
			in:      `{"status":"error-notFound","data":{}}`,
			wantErr: errUnexpectedRes,
		},
		{
			in:   `{"status":"ok","data":{"contents":{"id":{"link":"test"}}}}`,
			want: []string{"test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.in, func(t *testing.T) {
			got, err := parseURLs([]byte(tt.in))
			if (err == nil) != (tt.wantErr == nil) || (tt.wantErr != nil && !errors.Is(err, tt.wantErr)) {
				t.Fatalf("error: want %q, got %q", tt.wantErr, err)
			}
			if diff := cmp.Diff(tt.want, got); diff != "" {
				t.Errorf("mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
