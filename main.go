package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

const (
	guestWebsiteToken = "12345"
)

func main() {
	verbose := flag.Bool("v", false, "print verbose logs")
	token := flag.String("t", "", "user token. If omitted, a new guest account is created")
	wsToken := flag.String("w", guestWebsiteToken, "website token. Defaults to guest")
	concurrent := flag.Int("c", 8, "run this many downloads concurrently")
	flag.Parse()

	if *token == "" {
		t, err := fetchGuest()
		if err != nil {
			log.Fatalf("Error creating guest token: %v", err)
		}
		*token = t
		if *verbose {
			log.Printf("Using guest token %q", t)
		}
	}

	dirs := flag.Args()

	for _, dir := range dirs {
		dir = lastURISegment(dir)

		if *verbose {
			log.Printf("Downloading %q", dir)
		}

		urls, err := fetchURLs(dir, *token, *wsToken)
		if err != nil {
			log.Printf("Error fetching content IDs for %q: %v", dir, err)
			continue
		}

		if *verbose {
			log.Printf("Downloading %d files in %q", len(urls), dir)
		}

		if err := os.Mkdir(dir, 0755); err != nil {
			log.Printf("Error creating %q dir: %v", dir, err)
		}

		urlc := make(chan string)
		var wg sync.WaitGroup

		for i := 0; i < *concurrent; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				for url := range urlc {
					if *verbose {
						log.Printf("Downloading %q", url)
					}

					dst := filepath.Join(dir, lastURISegment(url))
					if err := dlFile(dst, url, *token); err != nil {
						log.Printf("Error downloading %q: %v", url, err)
					}
				}
			}()
		}

		for _, url := range urls {
			urlc <- url
		}
		close(urlc)

		wg.Wait()
	}
}

func dlFile(path, url, token string) error {
	file, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("Create: %w", err)
	}
	defer file.Close()

	if err := fetchFile(file, url, token); err != nil {
		return fmt.Errorf("fetchFile: %w", err)
	}

	return nil
}

func lastURISegment(s string) string {
	if len(s) == 0 {
		return s
	}

	segments := strings.Split(s, "/")
	return segments[len(segments)-1]
}
